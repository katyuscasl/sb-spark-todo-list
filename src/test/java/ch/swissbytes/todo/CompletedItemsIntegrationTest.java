package ch.swissbytes.todo;

import ch.swissbytes.todo.model.ToDoItem;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

// Mockito docs here: https://github.com/mockito/mockito
public class CompletedItemsIntegrationTest {
    private static final ToDoList app = new ToDoList();
    private static EntityManager mockEntityManager;

    @BeforeClass
    public static void setup() throws InterruptedException {
        mockEntityManager = mock(EntityManager.class);
        List<ToDoItem> completedList = new ArrayList<>();
        completedList.add(new ToDoItem(1, "Complete One", "desc"));
        completedList.add(new ToDoItem(2, "Complete Two", "desc"));
        when(mockEntityManager.getCompletedItems()).thenReturn(completedList);
        when(mockEntityManager.addNewItem(anyString(), anyString())).thenReturn(new ToDoItem(0, "", ""));

        app.startup(mockEntityManager);
        Thread.sleep(700);//let's give it some time to start the server-socket, for slower machines
    }

    @AfterClass
    public static void tearDown() {
        app.shutdown();
    }

    @Test
    public void willListTodoItems() {
        HelperMethods.UrlResponse response;
        response = HelperMethods.doMethod("GET", "/items", null);

        String body = response.body;
        assertThat(body, containsString("Complete One"));
        assertThat(body, containsString("Complete Two"));
        verify(mockEntityManager, times(1)).getCompletedItems();
    }
}
